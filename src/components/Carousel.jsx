import React, {useState} from 'react';
import '../assets/style/components/Carousel.css';

function Carousel(props) {

    const [foucsIdx, setFoucsIdx] = useState(0);

    function nextImg(){
        if(foucsIdx === (props.children.length - 1)){
            setFoucsIdx(0);
        }else{
            setFoucsIdx(foucsIdx + 1);
        }
    }

    
    function prevImg(){
        if(foucsIdx === 0){
            setFoucsIdx(props.children.length - 1);
        }else{
            setFoucsIdx(foucsIdx - 1);
        }
    }

    return (
        <div className="carousel-component">
            <div className="carousel-header">
               {props.children[foucsIdx].props.name}
            </div>
            <button className="carousel-btn-left" onClick={prevImg}>
                <i className="fas fa-arrow-circle-left"></i>
            </button>
            <button className="carousel-btn-right" onClick={nextImg}>
                <i className="fas fa-arrow-circle-right"></i>
            </button>
            {props.children[foucsIdx]}
        </div>
    );
}

export default Carousel;